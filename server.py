#!/usr/bin/env python
# -*- coding: utf-8 -*-

import socket
from socket import AF_INET, SOCK_STREAM, SO_REUSEADDR, SOL_SOCKET, SHUT_RDWR
import ssl
from subprocess import call
import hashlib

SERVER_IP = '127.0.0.1'
SERVER_PORT = 9000
KEYFILE = 'server.key'
CERTFILE = 'server.crt'
PASSWD = hashlib.sha256(("password").encode('utf-8')).hexdigest()

def echo_client(s):
    while True:
        data = s.recv(1024)
        if not data:
            break
        data = data.decode('utf-8')
        if PASSWD == data:
            print('Good password')
            s.send(b'ok')
            message = s.recv(1024)
            message = sms.decode('utf-8')
            print(message)
            call(["/bin/echo", message])
        else:
            print('Bad password')
            s.send(b'close')
        print('Connection closed')
    s.close()

def echo_server(address, port):
    s = socket.socket(AF_INET, SOCK_STREAM)
    s.bind((address, port))
    s.listen(1) # max backlog of connections
    s.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)

    s_ssl = ssl.wrap_socket(s, keyfile=KEYFILE, certfile=CERTFILE, server_side=True)

    while True:
        try:
            (c,a) = s_ssl.accept()
            print('Accept connection from {}:{}'.format(a[0], a[1]))
            echo_client(c)
        except socket.error as e:
            print('Error: {0}'.format(e))

echo_server(SERVER_IP, SERVER_PORT)
#!/usr/bin/env python
# -*- coding: utf-8 -*-

import socket
import ssl
import hashlib

SERVER_IP = "127.0.0.1"
SERVER_PORT = 9000
PASSWD = hashlib.sha256(("password").encode('utf-8')).hexdigest()

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# Require a certificate from the server. We used a self-signed certificate
# so here ca_certs must be the server certificate itself.
ssl_sock = ssl.wrap_socket(s,cert_reqs=ssl.CERT_REQUIRED, ca_certs='server.crt')
ssl_sock.connect((SERVER_IP, SERVER_PORT))
ssl_sock.send(str(PASSWD).encode())
data = ssl_sock.recv(4096)
data = data.decode('utf-8')
print(data)
if data == 'ok':
    ssl_sock.send(str('0011223344§Test').encode())
ssl_sock.close()